from importlib import reload
import os, sys
import bpy
from random import randint

bpy.context.preferences.view.show_tooltips_python=True

scene_data = {  'seed': randint(0,1000000), #4,
                'scene_complexity' : 50000000, #[1,100],
                'res' : [1080,1080],
                'engine':'BLENDER_EEVEE', #'CYCLES',
                'auto_save':False}#,
                #'post_history':folders.f_post_history}


os.system("cls")
path = 'F:\\Projects\\cosmogony\\'

#project = os.path.abspath(os.curdir)
if path not in sys.path: sys.path.append(path)

#print(project)

import Utilities.FolderStructure
import Utilities.Debug
import Modules.Ops
import Utilities.SeededRandom
import Modules.Cameras.CameraManager
import Modules.Scenes.SceneManager

reload(Utilities.FolderStructure)
reload(Modules.Ops)
reload(Utilities.Debug)
reload(Utilities.SeededRandom)
reload(Modules.Cameras.CameraManager)
reload(Modules.Scenes.SceneManager)

d = Utilities.Debug.DebugClass().debug
_d = [True,'--','MAIN']
folders = Utilities.FolderStructure.FolderStructureClass(os, path, d, _d)

r = Utilities.SeededRandom.SeededRandomClass(9)
gen_ops = Modules.Ops.OpsClass(bpy, d, _d)
cam_man = Modules.Cameras.CameraManager.CameraManagerClass(bpy, r, d, _d)
sce_man = Modules.Scenes.SceneManager.SceneManagerClass(bpy, r, folders.f_obj, d, _d)

def DrawScene():
    gen_ops.ClearScene()
    cam_man.AddCam(scene_data['scene_complexity'])
    sce_man.BuildScene(scene_data['scene_complexity'])
    # draw scene
    # draw materials
    # add lights


DrawScene()
