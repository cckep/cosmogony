class DebugClass():
    """master print command"""

    def __init__(self):
        super(DebugClass, self).__init__()

    def debug(self, d, f, n, v):
        """f-function, n-name, v-value"""
        if d[0]: print('%s %s : %s : %s: %s' % (d[1], d[2], f, n, v) )
        #if d: print('%s %s : %s -- %s: %s' % (f, n, v) )
