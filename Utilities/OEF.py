class OEFClass():

    def __init__(self, DEBUG, _d):
        self.debug = DEBUG
        _d = [_d[0],_d[1] + ' --', 'OEF']
        self._d = _d

        self.obj = {
        'SCENE_NAME' : 'NONE',
        'OBJECT_TYPE': 'NONE',
        'OBJECT_LINK': 'NONE',
        'SCENE_COUNT': 'NONE',
        'IS_UNIFORM' : 'NONE',
        'MIN_VERTS': 'NONE'
        }
        super(OEFClass, self).__init__()

    def __is_valid_key(self, d, k):
        if type(d.get(k)) != type(None):
            return True
        else:
            return False

    def Update(self, **kwargs):
        for k, v in kwargs.items():
            if self.__is_valid_key(self.obj, k):    #Make sure it's there
                self.obj[k] = v
                self.debug(self._d, 'Update()', k, v)
            else:
                self.debug(self._d, 'Update()', 'Cannot Find', k)

    def GetAttribute(self,a):
        if self.__is_valid_key(self.obj, a):    #Make sure it's there
            return self.obj[a]
        else: return ''

    def PrintAttr(self):
        for i in self.obj:
            self.debug(self._d, 'PrintAttr()', i, self.obj[i])
