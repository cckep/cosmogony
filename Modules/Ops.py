
class OpsClass():

    def __init__(self, bpy, DEBUG, _d):
        self.debug = DEBUG
        _d = [_d[0],_d[1] + ' --', 'OPS']
        self._d = _d
        self.bpy = bpy
        super(OpsClass,self).__init__()


    def ClearScene(self):
        try:
            for i in range(1, len(self.bpy.context.object.material_slots)):
                self.bpy.context.object.active_material_index = 1
                self.bpy.ops.object.material_slot_remove()
        except:
            self.debug(self._d, 'ClearScene()', 'Objects to load', 'NONE')


        for ob in self.bpy.data.objects:
            if ob.type == 'CAMERA':
                ob.select_set(state=False)
            else:
                ob.select_set(state=True)
        self.bpy.ops.object.delete()
        self.debug(self._d, 'ClearScene()', 'COMPLETE', 'Deleting Objects')

        for mat in self.bpy.data.materials:
            self.bpy.data.materials.remove(mat)
        self.debug(self._d, 'ClearScene()', 'COMPLETE', 'Deleting Materials')
        self.debug(self._d, 'ClearScene()', 'COMPLETE', 'Scene Clear')
