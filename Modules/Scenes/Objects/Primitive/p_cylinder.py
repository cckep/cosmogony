import bpy
import sys
from random import randint, uniform
#from Utilities.SeededRandom import SeededRandomClass
#

# --CREATES A MESH--
# ------------------
# -provide currency-
# returns the object
# ------------------
# loc = (0,0,0)-----
# rot = (0,0,0)-----
# size = (1,1,1)----
# dim = (<1,<1,<1)--
# ------------------
class P_CylinderClass():

    def __debug(self, f, n, v):
        if self.debug:
            print('%s %s -- %s -- %s: %s' % (self.inset, self.cname, f, n, v) )

    def __init__(self, r, _FPATH = '', _DEBUG = False, _INSET = ''):
        self.inset = _INSET + '-- '
        self.cname = 'PCUB'
        self.debug = _DEBUG

        self.r = r
        self.name = 'P_CYLINDER'
        self.count = 1
        self.vert_count = 8
        self.vertices = self.r.RandomInt([3,32])
        self.radius = self.r.RandomUnif([0.1,5])
        self.depth = self.r.RandomUnif([0.1,8])
        self.calc_uvs = self.r.RandomInt([1,1])
        if (self.vertices <= 20):
            self.subdivide = True
        else: self.subdivide = False
##        self.attr = {
##            'vertices': self.vertices,
##            'radius': self.radius,
##            'depth': self.depth,
##            'calc_uvs': self.calc_uvs
##            }
        super(P_CylinderClass, self).__init__()

    def __set_volume_to_one(self):
        ob = bpy.context.scene.objects[0]
        ##
        vol = ob.dimensions.x * ob.dimensions.y * ob.dimensions.z
        n = (1/vol)**(1/3.0)
        temp_v = list(i*n for i in ob.dimensions)

        ob.dimensions = temp_v
        bpy.ops.object.transform_apply(scale=True)

    def __vert_count(self):
        if self.subdivide:
            return self.count * (self.vertices*60 + 2) + (self.vertices*28) # with bevel/subdivide
        else:
            return self.count * self.vertices * 7
        #return self.count * self.vertices * 2 * 3

    def Name(self):
        return self.name

    def GetVerts(self, complexity, count = 1):
        #Vert count
        self.count = count
        return self.__vert_count()

    def GetObject(self, complexity):
    #bpy.ops.mesh.primitive_cylinder_add(
    #       vertices=32,
    #       radius=1,
    #       depth=2,
    #       end_fill_type='NGON',
    #       calc_uvs=False,
    #       view_align=False,
    #       enter_editmode=False,
    #       location=(0, 0, 0),
    #       rotation=(0, 0, 0),
    #       layers=(False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))
        self.attr = {
            'vertices': self.vertices,
            'radius': self.radius,
            'depth': self.depth,
            'calc_uvs': self.calc_uvs
            }
        bpy.ops.mesh.primitive_cylinder_add(**self.attr)
        ob = bpy.context.selected_objects[0]

        bpy.ops.object.shade_smooth()

        bev = ob.modifiers.new('Bevel', type = 'BEVEL')
        bev.width = 0.01
        bev.segments = 2
        if self.subdivide:
            bev.limit_method = 'ANGLE'
            subs = ob.modifiers.new('Subsurf', type = 'SUBSURF')

        print(ob.name_full)

#        for f in ob.data.polygons:
#            f.use_smooth = True

#        if self.bev_segments > 0:
#            bev = ob.modifiers.new('Bevel', type = 'BEVEL')
#            bev.width = 0.01
#            bev.segments = self.bev_segments

        self.__debug('GetObject()', 'Built Cylinder.py', 'COMPLETE')
        return ob
