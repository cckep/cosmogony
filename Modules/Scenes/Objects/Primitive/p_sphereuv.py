import bpy
import sys
from random import randint, uniform
from Utilities.SeededRandom import SeededRandomClass
#

#bpy.ops.mesh.primitive_uv_sphere_add(
#       segments=32,
#       ring_count=16,
#       size=1,
#       calc_uvs=False,
#       view_align=False,
#       enter_editmode=False,
#       location=(0, 0, 0),
#       rotation=(0, 0, 0),
#       layers=(False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))

# ------------------
class P_SphereUVClass():

    def __debug(self, f, n, v):
        if self.debug:
            print('%s %s -- %s -- %s: %s' % (self.inset, self.cname, f, n, v) )

    def __init__(self, r, _FPATH = '', _DEBUG = False, _INSET = ''):
        self.inset = _INSET + '-- '
        self.cname = 'PICO'
        self.debug = _DEBUG

        self.r = r
        self.name = 'P_SPHERE_ICO'
        self.count = 1
        self.vert_count = 8

        self.segments = 3
        self.ring_count = 3
        self.bevel = 0
        self.calc_uvs = 1
        self.size = 1 #self.r.RandomUnif([0.5,1.5])

        super(P_SphereUVClass, self).__init__()

    def Name(self):
        return self.name

    def GetVerts(self, complexity, count = 1):
        self.segments = self.r.RandomInt([3,32])
        self.ring_count = self.r.RandomInt([3,16])
        self.bevel = self.r.RandomInt([0,1])
        self.size = self.r.RandomUnif([0.5,1.5])
        #Vert count
        self.__debug('GetVerts()', 'Getting Verts', count)
        self.count = count

        if self.bevel:
            self.vert_count = (self.ring_count-1)*self.segments*4 + 2*self.segments
        else:
            self.vert_count =  2 + self.segments*(self.ring_count-1)
        return self.vert_count * self.count

    def GetObject(self, complexity):

        self.attr = {
            'segments' : self.segments,
            'ring_count': self.ring_count,
            'radius': self.size,
            'calc_uvs': self.calc_uvs
            }
        bpy.ops.mesh.primitive_uv_sphere_add(**self.attr)
        bpy.ops.object.shade_smooth()

        ob = bpy.context.selected_objects[0]
        print(ob.name_full)
#        for f in ob.data.polygons:
#            f.use_smooth = True

        if self.bevel:
            bev = ob.modifiers.new('Bevel', type = 'BEVEL')
            bev.width = 0.01
            bev.segments = self.bevel

        self.__debug('GetObject()', 'Built ICO Sphere.py', 'COMPLETE')
        return ob
